//
//  MainViewController.swift
//  PKInstagramKit
//
//  Created by Gokhan Gultekin on 10/10/2016.
//  Copyright © 2016 Peakode. All rights reserved.
//

import UIKit
import OAuthSwift
import Alamofire
import SwiftyJSON
import AlamofireImage

class MainViewController: UIViewController {

    @IBOutlet var logoutButton: UIButton!
    @IBOutlet var loginButton: UIButton!

    @IBOutlet var userView: UIView!

    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var fullnameLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var bioLabel: UILabel!
    @IBOutlet var followersLabel: UILabel!
    @IBOutlet var followingLabel: UILabel!
    @IBOutlet var mediaLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!

    @IBOutlet var userViewTopSpaceLayoutConstraint: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        
        getInstagramProfileWithToken(token: PKInstagramEngine.accessToken())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        makeInterface()
        
    }
    
    func makeInterface() {
        
        self.title = "PKInstagramKit"

        self.navigationController?.navigationBar.isTranslucent = false
        
        loginButton.layer.cornerRadius = 25
        logoutButton.layer.cornerRadius = 25

        loginButton.alpha = 0
        logoutButton.alpha = 0

        self.userViewTopSpaceLayoutConstraint.constant = 0
        self.userView.alpha = 0

    }

    @IBAction func logoutFromInstagram() {

        PKInstagramEngine.removeLocalData()
        
        presentLoginScreen()
    }
    
    @IBAction func presentLoginScreen() {
    
        let loginViewController = LoginViewController(nibName:"LoginViewController", bundle: nil)
        let navigationController: UINavigationController = UINavigationController(rootViewController: loginViewController)
        self.present(navigationController, animated: true, completion: nil)

    }
    
    func getInstagramProfileWithToken(token: String) {
        
        let urlString: String = "https://api.instagram.com/v1/users/self/?access_token=" + token
        
        Alamofire.request(urlString, method: .get, parameters:nil, encoding: URLEncoding.default , headers:nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                
                let resp = JSON(value)
                
                print("[PKInstagramKit] Response: \(resp)")
                
                if let code: Int = resp["meta"]["code"].int {
                    
                    if code == 400 {
                        
                        print("[PKInstagramKit] Error: \(resp["meta"]["error_message"])")
                        
                        //self.presentLoginScreen()
                        
                        self.setInterfaceIsLoggedIn(isLoggedIn: false)
                    
                        
                    } else {
                        
                        self.setInterfaceIsLoggedIn(isLoggedIn: true)
                        
                        print("[PKInstagramKit] Success: \(value)")
                        
                        self.makeUserViewWithData(data: value)
                    }
                }
                
                
            case .failure(let error):
                print(error)

                self.setInterfaceIsLoggedIn(isLoggedIn: false)

            }
        }

    }

    func setInterfaceIsLoggedIn(isLoggedIn: Bool) {
        
        if isLoggedIn == true {
            
            self.loginButton.alpha = 0
            self.logoutButton.alpha = 1
            
            self.userViewTopSpaceLayoutConstraint.constant = 0
            self.userView.alpha = 1

        } else {
            
            self.loginButton.alpha = 1
            self.logoutButton.alpha = 0

            self.userViewTopSpaceLayoutConstraint.constant = 90
            self.userView.alpha = 0

        }
    }

    func makeUserViewWithData(data: Any) {
        
        let resp = JSON(data)
    
        let urlString: String = resp["data"]["profile_picture"].string!
        let url: URL = URL(string: urlString)!

        profilePicture.af_setImage(withURL: url)
        
        fullnameLabel.text = resp["data"]["full_name"].string!
        usernameLabel.text = resp["data"]["username"].string!
        followersLabel.text = resp["data"]["counts"]["followed_by"].stringValue + " followers"
        followingLabel.text = resp["data"]["counts"]["follows"].stringValue + " following"
        mediaLabel.text = resp["data"]["counts"]["media"].stringValue + " media"
        websiteLabel.text = resp["data"]["website"].stringValue

        profilePicture.layer.cornerRadius = 40
        profilePicture.layer.masksToBounds = true

    }
    


    /*
    func getStuffFromInstagramWithAccessToken(token: String) {
        
        let urlString: String = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + token
        
        Alamofire.request(urlString, method: .get, parameters:nil, encoding: URLEncoding.default , headers:nil).responseJSON { response in
    
            switch response.result {
            case .success(let value):
                
                let resp = JSON(value)
                
                if let code: Int = resp["meta"]["code"].int {
                    
                    if code == 400 {
                        
                        print("[PKInstagramKit] Error: \(resp["meta"]["error_message"])")
                        
                        self.presentLoginScreen()
                        
                    } else {
                        
                        print("[PKInstagramKit] Success: \(value)")
                        
                    }
                }

        
            case .failure(let error):
                print(error)
            }
        }
    }
     */

}
