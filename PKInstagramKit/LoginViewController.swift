//
//  LoginViewController.swift
//  PKInstagramKit
//
//  Created by Gokhan Gultekin on 10/10/2016.
//  Copyright © 2016 Peakode. All rights reserved.
//

import UIKit
import OAuthSwift

class LoginViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PKInstagramEngine.removeLocalData()
        
        self.title = "Instagram"
        
        let dismissButton: UIBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action: #selector(self.dismissSelf))
        self.navigationItem.leftBarButtonItem = dismissButton
        
        webView.loadRequest(URLRequest.init(url: PKInstagramEngine.oauthURL()))
        
    }
    
    func dismissSelf() {
    
        self.dismiss(animated: true, completion: nil)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let urlString: String! = request.url?.absoluteString;
        
        if urlString.range(of: "access_token") != nil {
            
            let tokenString = urlString.components(separatedBy: "access_token=")
            
            let token: String = tokenString.last! as String
            PKInstagramEngine.saveAccessToken(token: token)
            
            self.dismiss(animated: true, completion: nil)
            
            print("token created successfully: \(token)")
            
        }
        
        return true
    }

}
